#include "Bullet.h"
#include "Tank.h"
#include "Turret.h"
#include <iostream>
using namespace std;

void Turret::setTurretX(float x)
{
	TurretX = x;
}

float Turret::getTurretX(void)
{
	return TurretX;
}

void Turret::setTurretY(float x)
{
	TurretY = x;
}

float Turret::getTurretY(void)
{
	return TurretY;
}

void Turret::setTurretAngle(float x)
{
	TurretAngle += x;
}

float Turret::getTurretAngle(void)
{
	return TurretAngle;
}

void Turret::setTurretTurnLeft(bool x)
{
	TurretTurnLeft = x;
}

bool Turret::getTurretTurnLeft(void)
{
	return TurretTurnLeft;
}

void Turret::setTurretTurnRight(bool x)
{
	TurretTurnRight = x;
}

bool Turret::getTurretTurnRight(void)
{
	return TurretTurnRight;
}

Turret::Turret()
{
	TurretX = 0.0f;
	TurretY = 0.0f;
	TurretAngle = 0.0f;
	TurretTurnLeft = false;
	TurretTurnRight = false;
}