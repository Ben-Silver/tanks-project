#include "Bullet.h"
#include "Tank.h"
#include <iostream>
using namespace std;

void Bullet::setBulletX(float bX)
{
	BulletX = bX;
}

float Bullet::getBulletX(void)
{
	return BulletX;
}
void Bullet::setBulletY(float bY)
{
	BulletY = bY;
}
float Bullet::getBulletY(void)
{
	return BulletY;
}
void Bullet::setBulletAngle(float bA)
{
	BulletAngle = bA;
}
float Bullet::getBulletAngle(void)
{
	return BulletAngle;
}
void Bullet::setBulletSpeed(float bS)
{
	BulletSpeed = bS;
}

float Bullet::getBulletSpeed(void)
{
	return BulletSpeed;
}

void Bullet::setSpawnBullet(bool sB)
{
	spawnBullet = sB;
}
bool Bullet::getSpawnBullet(void)
{
	return spawnBullet;
}

Bullet::Bullet()
{
	BulletX		= 100.0f;
	BulletY		= 100.0f;
	BulletAngle = 0.0f;
	BulletSpeed = 0.0f;
	spawnBullet = false;
}