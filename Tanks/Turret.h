#pragma once
#include "Tank.h"
#include <string>
using namespace std;

class Turret
{
	public:
		Turret();
		void setTurretX(float x);
		float getTurretX(void);
		void setTurretY(float x);
		float getTurretY(void);
		void setTurretAngle(float x);
		float getTurretAngle(void);
		void setTurretTurnLeft(bool x);
		bool getTurretTurnLeft(void);
		void setTurretTurnRight(bool x);
		bool getTurretTurnRight(void);

	private:
		float TurretX;
		float TurretY;
		float TurretAngle;
		bool TurretTurnLeft;
		bool TurretTurnRight;
};