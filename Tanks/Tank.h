#pragma once
#include <string>
#include "Bullet.h"
#include "Turret.h"
using namespace std;

class tankPlayer
{
	public:
		tankPlayer();
		void setPlayerX(float x);
		float getPlayerX(void);
		void setPlayerY(float x);
		float getPlayerY(void);
		void setPlayerAngle(float x);
		float getPlayerAngle(void);
		void setPlayerSpeed(float x);
		float getPlayerSpeed(void);
		void setSpeedUp(bool sU);
		bool getSpeedUp(void);
		void setSlowDown(bool sD);
		bool getSlowDown(void);
		void setTurnLeft(bool tL);
		bool getTurnLeft(void);
		void setTurnRight(bool tR);
		bool getTurnRight(void);
		void setFireGun(bool fG);
		bool getFireGun(void);
		void setWPressed(bool wP);
		bool getWPressed(void);
		void setSPressed(bool sP);
		bool getSPressed(void);
		Bullet * tankBullet;
		Turret * tankTurret;

	private:
		float PlayerX;
		float PlayerY;
		float PlayerAngle;
		float PlayerSpeed;
		bool speedUp;
		bool slowDown;
		bool turnLeft;
		bool turnRight;
		bool fireGun;
		bool WPressed;
		bool SPressed;
};