#pragma once
#include "Tank.h"
#include <string>
using namespace std;

class Bullet
{
	public:
		Bullet();
		void setBulletX(float x);
		float getBulletX(void);
		void setBulletY(float x);
		float getBulletY(void);
		void setBulletAngle(float x);
		float getBulletAngle(void);
		void setBulletSpeed(float x);
		float getBulletSpeed(void);
		void setSpawnBullet(bool x);
		bool getSpawnBullet(void);

	private:
		float BulletX;
		float BulletY;
		float BulletAngle;
		float BulletSpeed;
		bool spawnBullet;
};