

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

// OpenGL without using GLUT - 2013 by Neil Dansey, Tim Dykes and Ian Cant, and using excerpts from here:
// http://bobobobo.wordpress.com/2008/02/11/opengl-in-a-proper-windows-app-no-glut/
// Feel free to adapt this for what you need, but please leave these comments in.

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

#pragma once

#include <windows.h>	// need this file if you want to create windows etc
#include <gl/gl.h>		// need this file to do graphics with opengl
#include <gl/glu.h>		// need this file to set up a perspective projection easily
#include <math.h>		// need this for maths functions including pow() and sqrt()
#include <string>
#include <stdlib.h>
#include <Windows.h>
#include "Tank.h"
#include <iostream>
using namespace std;

// include the opengl and glu libraries
#pragma comment(lib, "opengl32.lib")	
#pragma comment(lib, "glu32.lib")

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

float tankVertices[18] = 
{
	-0.15f, -0.1f, 0.0f,				// Vertex 1 (x, y, z)
	0.15f, -0.1f, 0.0f,					// Vertex 2 (x, y, z)
	-0.15f, 0.1f, 0.0f,					// Vertex 3 (x, y, z)
	-0.15f, 0.1f, 0.0f,					// Vertex 4 (x, y, z) - Added
	0.15f, -0.1f, 0.0f,					// Vertex 5 (x, y, z) - Added
	0.15f, 0.1f, 0.0f					// Vertex 6 (x, y, z) - Added
};

float turretVertices[2][18] =
{
	{ -0.04f, -0.04f, 0.0f,				// Vertex 1 (x, y, z)
	0.15f, -0.04f, 0.0f,				// Vertex 2 (x, y, z)
	-0.04f, 0.04f, 0.0f,				// Vertex 3 (x, y, z)
	-0.04f, 0.04f, 0.0f,				// Vertex 4 (x, y, z)
	0.15f, -0.04f, 0.0f,				// Vertex 5 (x, y, z)
	0.15f, 0.04f, 0.0f },				// Vertex 6 (x, y, z)

	{-0.15f, -0.04f, 0.0f,				// Vertex 1 (x, y, z)
	0.04f, -0.04f, 0.0f,				// Vertex 2 (x, y, z)
	-0.15f, 0.04f, 0.0f,				// Vertex 3 (x, y, z)
	-0.15f, 0.04f, 0.0f,				// Vertex 4 (x, y, z)
	0.04f, -0.04f, 0.0f,				// Vertex 5 (x, y, z)
	0.04f, 0.04f, 0.0f }				// Vertex 6 (x, y, z)
};

float bulletVertices[18] = 
{
	-0.04f, -0.04f, 0.0f,				// Vertex 1 (x, y, z)
	0.04f, -0.04f, 0.0f,				// Vertex 2 (x, y, z)
	-0.04f, 0.04f, 0.0f,				// Vertex 3 (x, y, z)
	-0.04f, 0.04f, 0.0f,				// Vertex 4 (x, y, z) - Added
	0.04f, -0.04f, 0.0f,				// Vertex 5 (x, y, z) - Added
	0.04f, 0.04f, 0.0f					// Vertex 6 (x, y, z) - Added
};

float tankVertexColors[2][24] = 
{
	{0.0f, 1.0f, 0.0f, 1.0f,			// Vertex 1 (x, y, z)
	0.0f, 1.0f, 0.0f, 1.0f,				// Vertex 2 (x, y, z)
	0.0f, 1.0f, 0.0f, 1.0f,				// Vertex 3 (x, y, z)
	0.0f, 1.0f, 0.0f, 1.0f,				// Vertex 4 (x, y, z) - Added
	0.0f, 1.0f, 0.0f, 1.0f,				// Vertex 5 (x, y, z) - Added
	0.0f, 1.0f, 0.0f, 1.0f},			// Vertex 6 (x, y, z) - Added

	{0.0f, 0.0f, 1.0f, 1.0f,			// Vertex 1 (x, y, z)
	0.0f, 0.0f, 1.0f, 1.0f,				// Vertex 2 (x, y, z)
	0.0f, 0.0f, 1.0f, 1.0f,				// Vertex 3 (x, y, z)
	0.0f, 0.0f, 1.0f, 1.0f,				// Vertex 4 (x, y, z) - Added
	0.0f, 0.0f, 1.0f, 1.0f,				// Vertex 5 (x, y, z) - Added
	0.0f, 0.0f, 1.0f, 1.0f}				// Vertex 6 (x, y, z) - Added
};

float turretVertexColors[2][24] = 
{
	{0.5f, 1.0f, 0.5f, 1.0f,			// Vertex 1 (x, y, z)
	0.5f, 1.0f, 0.5f, 1.0f,				// Vertex 2 (x, y, z)
	0.5f, 1.0f, 0.5f, 1.0f,				// Vertex 3 (x, y, z)
	0.5f, 1.0f, 0.5f, 1.0f,				// Vertex 4 (x, y, z)
	0.5f, 1.0f, 0.5f, 1.0f,				// Vertex 5 (x, y, z)
	0.5f, 1.0f, 0.5f, 1.0f},			// Vertex 6 (x, y, z)

	{0.5f, 0.5f, 1.0f, 1.0f,			// Vertex 1 (x, y, z)
	0.5f, 0.5f, 1.0f, 1.0f,				// Vertex 2 (x, y, z)
	0.5f, 0.5f, 1.0f, 1.0f,				// Vertex 3 (x, y, z)
	0.5f, 0.5f, 1.0f, 1.0f,				// Vertex 4 (x, y, z)
	0.5f, 0.5f, 1.0f, 1.0f,				// Vertex 5 (x, y, z)
	0.5f, 0.5f, 1.0f, 1.0f}				// Vertex 6 (x, y, z)
};

float bulletColours[24] =
{
	0.0f, 0.0f, 0.0f, 1.0f,				// Vertex 1 (x, y, z)
	0.0f, 0.0f, 0.0f, 1.0f,				// Vertex 2 (x, y, z)
	0.0f, 0.0f, 0.0f, 1.0f,				// Vertex 3 (x, y, z)
	0.0f, 0.0f, 0.0f, 1.0f,				// Vertex 4 (x, y, z) - Added
	0.0f, 0.0f, 0.0f, 1.0f,				// Vertex 5 (x, y, z) - Added
	0.0f, 0.0f, 0.0f, 1.0f				// Vertex 6 (x, y, z) - Added
};

struct Game
{
	tankPlayer * Tank = new tankPlayer[2];

	Game()
	{

	}
};

// function prototypes:
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam);
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdLine, int iCmdShow);
bool detectCollision(float x1, float y1, float x2, float y2, float threshhold);							// Collision Detection Prototype
bool wrapScreen(float x1, float x2, float y1, float y2);												// Screen Wrap Prototype
void draw(Game * mainGame);																				// Draw prototype
void update(Game * mainGame);																			// Update prototype


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

// In a C++ Windows app, the starting point is WinMain() rather than _tmain() or main().
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdLine, int iCmdShow)
{
	// some basic numbers to hold the position and size of the window
	int windowWidth		= 800;
	int windowHeight	= 600;
	int windowTopLeftX	= 50;
	int windowTopLeftY	= 50;

	// some other variables we need for our game...
	MSG msg;								// this will be used to store messages from the operating system
	bool keepPlaying = true;				// whether or not we want to keep playing



	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	// this section contains all the window initialisation code, 
	// and should probably be collapsed for the time being to avoid confusion	
#pragma region  <-- click the plus/minus sign to collapse/expand!

	// this bit creates a window class, basically a template for the window we will make later, so we can do more windows the same.
	WNDCLASS myWindowClass;
	myWindowClass.cbClsExtra = 0;											// no idea
	myWindowClass.cbWndExtra = 0;											// no idea
	myWindowClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);		// background fill black
	myWindowClass.hCursor = LoadCursor(NULL, IDC_ARROW);					// arrow cursor       
	myWindowClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);					// type of icon to use (default app icon)
	myWindowClass.hInstance = hInstance;									// window instance name (given by the OS when the window is created)   
	myWindowClass.lpfnWndProc = WndProc;									// window callback function - this is our function below that is called whenever something happens
	myWindowClass.lpszClassName = TEXT("my window class name");				// our new window class name
	myWindowClass.lpszMenuName = 0;											// window menu name (0 = default menu?) 
	myWindowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;				// redraw if the window is resized horizontally or vertically, allow different context for each window instance

	// Register that class with the Windows OS..
	RegisterClass(&myWindowClass);

	// create a rect structure to hold the dimensions of our window
	RECT rect;
	SetRect(&rect, windowTopLeftX,				// the top-left corner x-coordinate
		windowTopLeftY,				// the top-left corner y-coordinate
		windowTopLeftX + windowWidth,		// far right
		windowTopLeftY + windowHeight);	// far left

	// adjust the window, no idea why.
	AdjustWindowRect(&rect, WS_OVERLAPPEDWINDOW, false);

	// call CreateWindow to create the window
	HWND myWindow = CreateWindow(TEXT("my window class name"),						// window class to use - in this case the one we created a minute ago
		TEXT("Tanks Game"),															// window title
		WS_OVERLAPPEDWINDOW,														// ??
		windowTopLeftX, windowTopLeftY,												// x, y
		windowWidth, windowHeight,													// width and height
		NULL, NULL,																	// ??
		hInstance, NULL);															// ??

	//Game mainGame;
	Game * mainGame = new Game[2];													// Game Pointer
	mainGame->Tank[0].setPlayerX(-1.0f);											// Initializing X Co-Ordinate of 1st Tank
	mainGame->Tank[0].setPlayerY(0.0f);												// Initializing Y Co-Ordinate of 1st Tank
	mainGame->Tank[0].setPlayerAngle(0.0f);											// Initializing Default Angle of 1st Tank
	mainGame->Tank[1].setPlayerX(1.0f);												// Initializing X Co-Ordinate of 2nd Tank
	mainGame->Tank[1].setPlayerY(0.0f);												// Initializing Y Co-Ordinate of 2nd Tank
	mainGame->Tank[1].setPlayerAngle(0.0f);											// Initializing Default Angle of 2nd Tank

	// check to see that the window created okay
	if (myWindow == NULL)
	{
		FatalAppExit(NULL, TEXT("CreateWindow() failed!")); // ch15
	}

	// if so, show it
	ShowWindow(myWindow, iCmdShow);


	// get a device context from the window
	HDC myDeviceContext = GetDC(myWindow);


	// we create a pixel format descriptor, to describe our desired pixel format. 
	// we set all of the fields to 0 before we do anything else
	// this is because PIXELFORMATDESCRIPTOR has loads of fields that we won't use
	PIXELFORMATDESCRIPTOR myPfd = { 0 };


	// now set only the fields of the pfd we care about:
	myPfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);		// size of the pfd in memory
	myPfd.nVersion = 1;									// always 1

	myPfd.dwFlags = PFD_SUPPORT_OPENGL |				// OpenGL support - not DirectDraw
		PFD_DOUBLEBUFFER |								// double buffering support
		PFD_DRAW_TO_WINDOW;								// draw to the app window, not to a bitmap image

	myPfd.iPixelType = PFD_TYPE_RGBA;					// red, green, blue, alpha for each pixel
	myPfd.cColorBits = 24;								// 24 bit == 8 bits for red, 8 for green, 8 for blue.
	// This count of color bits EXCLUDES alpha.

	myPfd.cDepthBits = 32;								// 32 bits to measure pixel depth.


	// now we need to choose the closest pixel format to the one we wanted...	
	int chosenPixelFormat = ChoosePixelFormat(myDeviceContext, &myPfd);

	// if windows didnt have a suitable format, 0 would have been returned...
	if (chosenPixelFormat == 0)
	{
		FatalAppExit(NULL, TEXT("ChoosePixelFormat() failed!"));
	}

	// if we get this far it means we've got a valid pixel format
	// so now we need to set the device context up with that format...
	int result = SetPixelFormat(myDeviceContext, chosenPixelFormat, &myPfd);

	// if it failed...
	if (result == NULL)
	{
		FatalAppExit(NULL, TEXT("SetPixelFormat() failed!"));
	}

	// we now need to set up a render context (for opengl) that is compatible with the device context (from windows)...
	HGLRC myRenderContext = wglCreateContext(myDeviceContext);

	// then we connect the two together
	wglMakeCurrent(myDeviceContext, myRenderContext);



	// opengl display setup
	glMatrixMode(GL_PROJECTION);	// select the projection matrix, i.e. the one that controls the "camera"
	glLoadIdentity();				// reset it
	gluPerspective(45.0, (float)windowWidth / (float)windowHeight, 1, 1000);	// set up fov, and near / far clipping planes
	glViewport(0, 0, windowWidth, windowHeight);							// make the viewport cover the whole window
	glClearColor(0.5, 0, 0, 1.0);											// set the colour used for clearing the screen

	// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#pragma endregion

	// main game loop starts here!


	// keep doing this as long as the player hasnt exited the app: 
	while (keepPlaying == true)
	{

		// we need to listen out for OS messages.
		// if there is a windows message to process...
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			// and if the message is a "quit" message...
			if (msg.message == WM_QUIT)
			{
				keepPlaying = false;	// we want to quit asap
			}
			else if (msg.message == WM_KEYDOWN)
			{
				// Player 1
				if (msg.wParam == 'W')
				{
					mainGame->Tank[0].setSlowDown(false);
					mainGame->Tank[0].setSpeedUp(true);
					mainGame->Tank[0].setWPressed(true);
				}
				else if (msg.wParam == 'A')
				{
					mainGame->Tank[0].setTurnLeft(true);
				}
				else if (msg.wParam == 'S')
				{
					mainGame->Tank[0].setSlowDown(false);
					mainGame->Tank[0].setSpeedUp(true);
					mainGame->Tank[0].setSPressed(true);
				}
				else if (msg.wParam == 'D')
				{
					mainGame->Tank[0].setTurnRight(true);
				}
				else if (msg.wParam == 'Q')
				{
					mainGame->Tank[0].tankTurret->setTurretTurnLeft(true);
				}
				else if (msg.wParam == 'E')
				{
					mainGame->Tank[0].tankTurret->setTurretTurnRight(true);
				}
				else if (msg.wParam == 'F')
				{
					mainGame->Tank[0].setFireGun(true);
				}

				// Player 2
				else if (msg.wParam == 'I')
				{
					mainGame->Tank[1].setSlowDown(false);
					mainGame->Tank[1].setSpeedUp(true);
					mainGame->Tank[1].setWPressed(true);
				}
				else if (msg.wParam == 'J')
				{
					mainGame->Tank[1].setTurnLeft(true);
				}
				else if (msg.wParam == 'K')
				{
					mainGame->Tank[1].setSlowDown(false);
					mainGame->Tank[1].setSpeedUp(true);
					mainGame->Tank[1].setSPressed(true);
				}
				else if (msg.wParam == 'L')
				{
					mainGame->Tank[1].setTurnRight(true);
				}
				else if (msg.wParam == 'U')
				{
					mainGame->Tank[1].tankTurret->setTurretTurnLeft(true);
				}
				else if (msg.wParam == 'O')
				{
					mainGame->Tank[1].tankTurret->setTurretTurnRight(true);
				}
				else if (msg.wParam == 'H')
				{
					mainGame->Tank[1].setFireGun(true);
				}
			}

			else if (msg.message == WM_KEYUP)
			{
				// Player 1
				if (msg.wParam == 'W')
				{
					mainGame->Tank[0].setSpeedUp(false); //speedUp = false;
					mainGame->Tank[0].setSlowDown(true); //slowDown = true;
					mainGame->Tank[0].setWPressed(false); //WPressed = false;
				}
				else if (msg.wParam == 'A')
				{
					mainGame->Tank[0].setTurnLeft(false); //turnLeft = false;
				}
				else if (msg.wParam == 'S')
				{
					mainGame->Tank[0].setSpeedUp(false); //speedUp = false;
					mainGame->Tank[0].setSlowDown(true); //slowDown = true;
					mainGame->Tank[0].setSPressed(false); //SPressed = false;
				}
				else if (msg.wParam == 'D')
				{
					mainGame->Tank[0].setTurnRight(false); //turnRight = false;
				}
				else if (msg.wParam == 'Q')
				{
					mainGame->Tank[0].tankTurret->setTurretTurnLeft(false);
				}
				else if (msg.wParam == 'E')
				{
					mainGame->Tank[0].tankTurret->setTurretTurnRight(false);
				}
				else if (msg.wParam == 'F')
				{
					mainGame->Tank[0].setFireGun(false);
				}

				// Player 2
				if (msg.wParam == 'I')
				{
					mainGame->Tank[1].setSpeedUp(false); //speedUp = false;
					mainGame->Tank[1].setSlowDown(true); //slowDown = true;
					mainGame->Tank[1].setWPressed(false); //WPressed = false;
				}
				else if (msg.wParam == 'J')
				{
					mainGame->Tank[1].setTurnLeft(false); //turnLeft = false;
				}
				else if (msg.wParam == 'K')
				{
					mainGame->Tank[1].setSpeedUp(false); //speedUp = false;
					mainGame->Tank[1].setSlowDown(true); //slowDown = true;
					mainGame->Tank[1].setSPressed(false); //SPressed = false;
				}
				else if (msg.wParam == 'L')
				{
					mainGame->Tank[1].setTurnRight(false); //turnRight = false;
				}
				else if (msg.wParam == 'U')
				{
					mainGame->Tank[1].tankTurret->setTurretTurnLeft(false);
				}
				else if (msg.wParam == 'O')
				{
					mainGame->Tank[1].tankTurret->setTurretTurnRight(false);
				}
				else if (msg.wParam == 'H')
				{
					mainGame->Tank[1].setFireGun(false);
				}
			}
			// or if it was any other type of message (i.e. one we don't care about), process it as normal...
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);		// clear screen
		update(mainGame);										// Function call for 'update'
		draw(mainGame);											// Function call for 'draw'
		SwapBuffers(myDeviceContext);							// update graphics

	}


	// the next bit will therefore happen when the player quits the app,
	// because they are trapped in the previous section as long as (keepPlaying == true).

	// UNmake our rendering context (make it 'uncurrent')
	wglMakeCurrent(NULL, NULL);

	// delete the rendering context, we no longer need it.
	wglDeleteContext(myRenderContext);

	// release our window's DC from the window
	ReleaseDC(myWindow, myDeviceContext);

	delete [] mainGame->Tank;																// Cleanup Code for mainGame
	mainGame->Tank = NULL;																	// Cleanup Code for mainGame

	delete mainGame;																		// Cleanup Code for mainGame
	mainGame = NULL;																		// Cleanup Code for mainGame

	// end the program
	return msg.wParam;
}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

// this part contains some code that should be collapsed for now too...
#pragma region keep_this_bit_collapsed_too!

// this function is called when any events happen to our window
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam)
{

	switch (message)
	{
		// if they exited the window...	
	case WM_DESTROY:
		// post a message "quit" message to the main windows loop
		PostQuitMessage(0);
		return 0;
		break;
	}

	// must do this as a default case (i.e. if no other event was handled)...
	return DefWindowProc(hwnd, message, wparam, lparam);

}

#pragma endregion

bool detectCollision(float x1, float y1, float x2, float y2, float threshhold) // Collision Detection
{
	float xDiff = x2 - x1;													   // Finding the 'x' distance between x2 and x1
	float yDiff = y2 - y1;													   // Finding the 'y' distance between y2 and y1

	float xSQR = pow(xDiff, 2);												   // Finding the value of the 'x' distance squared
	float ySQR = pow(yDiff, 2);												   // Finding the value of the 'y' distance squared

	float total = xSQR + ySQR;												   // Finding the total value of the 'xSQR' distance + the 'ySQR' distance
	float dist = sqrt(total); 												   // Finding the square root value of the 'xSQR' distance + the 'ySQR' distance

	if (dist < threshhold)													   // Comparing the variables 'dist' and 'threshold' to determine the outcome
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool wrapScreen(float x, float y)
{
	float boundaryX1 = -3.0f;												   // The Maximum X Value in the Left Direction
	float boundaryX2 = 3.0f;												   // The Maximum X Value in the Right Direction
	float boundaryY1 = -2.2f;												   // The Maximum Y Value in the Downwards Direction
	float boundaryY2 = 2.2f;												   // The Maximum Y Value in the Upwards Direction

	// X Boundary
	if (x < boundaryX1)														   // Checks to see if Tank[0] has exeeded the first X boundary
	{
		return true;														   // If it has it returns true
	}

	else if (x > boundaryX2)												   // Checks to see if Tank[0] has exeeded the second X boundary
	{
		return true;														   // If it has it returns true
	}

	// Y Boundary
	else if (y < boundaryY1)												   // Checks to see if Tank[1] has exeeded the first X boundary
	{
		return true;														   // If it has it returns true
	}

	else if (y > boundaryY2)												   // Checks to see if Tank[1] has exeeded the second X boundary
	{
		return true;														   // If it has it returns true
	}
	else
	{
		return false;
	}
}

void draw(Game * mainGame)
{
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);
	glMatrixMode(GL_MODELVIEW);

	for (int i = 0; i <= 1; i++)
	{

		glVertexPointer(3, GL_FLOAT, 0, tankVertices);
		glColorPointer(4, GL_FLOAT, 0, tankVertexColors[i]);
		glLoadIdentity();
		glTranslatef(mainGame->Tank[i].getPlayerX(), mainGame->Tank[i].getPlayerY(), -5.0);
		glRotatef(mainGame->Tank[i].getPlayerAngle(), 0.0, 0.0, 1.0);
		glDrawArrays(GL_TRIANGLES, 0, 6);

		glVertexPointer(3, GL_FLOAT, 0, bulletVertices);
		glColorPointer(4, GL_FLOAT, 0, bulletColours);
		glLoadIdentity();
		glTranslatef(mainGame->Tank[i].tankBullet->getBulletX(), mainGame->Tank[i].tankBullet->getBulletY(), -5.0);
		glRotatef(mainGame->Tank[i].tankBullet->getBulletAngle(), 0.0, 0.0, 1.0);
		glDrawArrays(GL_TRIANGLES, 0, 6);

		glVertexPointer(3, GL_FLOAT, 0, turretVertices[i]);
		glColorPointer(4, GL_FLOAT, 0, turretVertexColors[i]);
		glLoadIdentity();
		glTranslatef(mainGame->Tank[i].getPlayerX(), mainGame->Tank[i].getPlayerY(), -5.0);
		glRotatef(mainGame->Tank[i].tankTurret->getTurretAngle(), 0.0, 0.0, 1.0);
		glDrawArrays(GL_TRIANGLES, 0, 6);
	}

}

void update(Game * mainGame)
{
	// Collision Detection
	bool result = detectCollision(mainGame->Tank[0].getPlayerX(), mainGame->Tank[0].getPlayerY(), mainGame->Tank[1].getPlayerX(), mainGame->Tank[1].getPlayerY(), 0.2f);
	bool P1Hit = detectCollision(mainGame->Tank[1].getPlayerX(), mainGame->Tank[1].getPlayerY(), mainGame->Tank[0].tankBullet->getBulletX(), mainGame->Tank[0].tankBullet->getBulletY(), 0.2f);
	bool P2Hit = detectCollision(mainGame->Tank[0].getPlayerX(), mainGame->Tank[0].getPlayerY(), mainGame->Tank[1].tankBullet->getBulletX(), mainGame->Tank[1].tankBullet->getBulletY(), 0.2f);
	if (result == true)
	{
		glClearColor(1.0, 1.0, 1.0, 1.0);
	}
	if (P1Hit == true)
	{
		glClearColor(1.0, 1.0, 1.0, 1.0);
	}
	if (P2Hit == true)
	{
		glClearColor(1.0, 1.0, 1.0, 1.0);
	}
	
	// Keys For Player 1
	if (mainGame->Tank[0].getSpeedUp() == true)
	{
		if (mainGame->Tank[0].getWPressed() == true)
		{
			mainGame->Tank[0].setPlayerSpeed(0.001f);
		}
		if (mainGame->Tank[0].getSPressed() == true)
		{
			mainGame->Tank[0].setPlayerSpeed(-0.001f);
		}
	}
	if (mainGame->Tank[0].getSlowDown() == true)
	{
		mainGame->Tank[0].setPlayerSpeed(0.0f);
	}
	if (mainGame->Tank[0].getTurnLeft() == true)
	{
		mainGame->Tank[0].setPlayerAngle(0.08f);
	}
	if (mainGame->Tank[0].getTurnRight() == true)
	{
		mainGame->Tank[0].setPlayerAngle(-0.08f);
	}

	// Keys For Player 2
	if (mainGame->Tank[1].getSpeedUp() == true)
	{
		if (mainGame->Tank[1].getWPressed() == true)
		{
			mainGame->Tank[1].setPlayerSpeed(-0.001f);
		}
		if (mainGame->Tank[1].getSPressed() == true)
		{
			mainGame->Tank[1].setPlayerSpeed(0.001f);
		}
	}
	if (mainGame->Tank[1].getSlowDown() == true)
	{
		mainGame->Tank[1].setPlayerSpeed(0.0f);
	}
	if (mainGame->Tank[1].getTurnLeft() == true)
	{
		mainGame->Tank[1].setPlayerAngle(0.08f);
	}
	if (mainGame->Tank[1].getTurnRight() == true)
	{
		mainGame->Tank[1].setPlayerAngle(-0.08f);
	}

	// Keys For Player 1 Turret Turning
	if (mainGame->Tank[0].tankTurret->getTurretTurnLeft() == true)
	{
		mainGame->Tank[0].tankTurret->setTurretAngle(0.08);
	}
	if (mainGame->Tank[0].tankTurret->getTurretTurnRight() == true)
	{
		mainGame->Tank[0].tankTurret->setTurretAngle(-0.08);
	}

	// Keys For Player 2 Turret Turning
	if (mainGame->Tank[1].tankTurret->getTurretTurnLeft() == true)
	{
		mainGame->Tank[1].tankTurret->setTurretAngle(0.08);
	}
	if (mainGame->Tank[1].tankTurret->getTurretTurnRight() == true)
	{
		mainGame->Tank[1].tankTurret->setTurretAngle(-0.08);
	}

	// Keys For Player 1 Firing
	if (mainGame->Tank[0].getFireGun() == true)
	{
		mainGame->Tank[0].tankBullet->setBulletX(mainGame->Tank[0].getPlayerX());
		mainGame->Tank[0].tankBullet->setBulletY(mainGame->Tank[0].getPlayerY());
		mainGame->Tank[0].tankBullet->setBulletAngle(mainGame->Tank[0].tankTurret->getTurretAngle());
		mainGame->Tank[0].tankBullet->setBulletSpeed(0.002f);
	}

	// Keys For Player 2 Firing
	if (mainGame->Tank[1].getFireGun() == true)
	{
		mainGame->Tank[1].tankBullet->setBulletX(mainGame->Tank[1].getPlayerX());
		mainGame->Tank[1].tankBullet->setBulletY(mainGame->Tank[1].getPlayerY());
		mainGame->Tank[1].tankBullet->setBulletAngle(mainGame->Tank[1].tankTurret->getTurretAngle());
		mainGame->Tank[1].tankBullet->setBulletSpeed(-0.002f);
	}

	// Moving for Player 1
	mainGame->Tank[0].setPlayerX(mainGame->Tank[0].getPlayerX() + (mainGame->Tank[0].getPlayerSpeed() * (cos(mainGame->Tank[0].getPlayerAngle() * 0.01745f))));
	mainGame->Tank[0].setPlayerY(mainGame->Tank[0].getPlayerY() + (mainGame->Tank[0].getPlayerSpeed() * (sin(mainGame->Tank[0].getPlayerAngle() * 0.01745f))));
	
	// Moving for Player 2
	mainGame->Tank[1].setPlayerX(mainGame->Tank[1].getPlayerX() + (mainGame->Tank[1].getPlayerSpeed() * (cos(mainGame->Tank[1].getPlayerAngle() * 0.01745f))));
	mainGame->Tank[1].setPlayerY(mainGame->Tank[1].getPlayerY() + (mainGame->Tank[1].getPlayerSpeed() * (sin(mainGame->Tank[1].getPlayerAngle() * 0.01745f))));

	// Moving for Bullet 1
	mainGame->Tank[0].tankBullet->setBulletX(mainGame->Tank[0].tankBullet->getBulletX() + (mainGame->Tank[0].tankBullet->getBulletSpeed() * (cos(mainGame->Tank[0].tankBullet->getBulletAngle() * 0.01745f))));
	mainGame->Tank[0].tankBullet->setBulletY(mainGame->Tank[0].tankBullet->getBulletY() + (mainGame->Tank[0].tankBullet->getBulletSpeed() * (sin(mainGame->Tank[0].tankBullet->getBulletAngle() * 0.01745f))));

	// Moving for Bullet 2
	mainGame->Tank[1].tankBullet->setBulletX(mainGame->Tank[1].tankBullet->getBulletX() + (mainGame->Tank[1].tankBullet->getBulletSpeed() * (cos(mainGame->Tank[1].tankBullet->getBulletAngle() * 0.01745f))));
	mainGame->Tank[1].tankBullet->setBulletY(mainGame->Tank[1].tankBullet->getBulletY() + (mainGame->Tank[1].tankBullet->getBulletSpeed() * (sin(mainGame->Tank[1].tankBullet->getBulletAngle() * 0.01745f))));

	// Screen Wrapper for Tank[0]
	bool offScreen1 = wrapScreen(mainGame->Tank[0].getPlayerX(), mainGame->Tank[0].getPlayerY());
	
	// X Boundary
	if ((offScreen1 == true) && (mainGame->Tank[0].getPlayerX() < -3.0f))
	{
		mainGame->Tank[0].setPlayerX(3.0f);
	}
	else if ((offScreen1 == true) && (mainGame->Tank[0].getPlayerX() > 3.0f))
	{
		mainGame->Tank[0].setPlayerX(-3.0f);
	}

	// Y Boundary
	else if ((offScreen1 == true) && (mainGame->Tank[0].getPlayerY() < -2.2f))
	{
		mainGame->Tank[0].setPlayerY(2.2f);
	}
	else if ((offScreen1 == true) && (mainGame->Tank[0].getPlayerY() > 2.2f))
	{
		mainGame->Tank[0].setPlayerY(-2.2f);
	}

	// Screen Wrapper for Tank[1]
	bool offScreen2 = wrapScreen(mainGame->Tank[1].getPlayerX(), mainGame->Tank[1].getPlayerY());

	// X Boundary
	if ((offScreen2 == true) && (mainGame->Tank[1].getPlayerX() < -3.0f))
	{
		mainGame->Tank[1].setPlayerX(3.0f);
	}
	else if ((offScreen2 == true) && (mainGame->Tank[1].getPlayerX() > 3.0f))
	{
		mainGame->Tank[1].setPlayerX(-3.0f);
	}

	// Y Boundary
	else if ((offScreen2 == true) && (mainGame->Tank[1].getPlayerY() < -2.2f))
	{
		mainGame->Tank[1].setPlayerY(2.2f);
	}
	else if ((offScreen2 == true) && (mainGame->Tank[1].getPlayerY() > 2.2f))
	{
		mainGame->Tank[1].setPlayerY(-2.2f);
	}
}