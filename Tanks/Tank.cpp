#include "Tank.h"
#include <iostream>
using namespace std;

void tankPlayer::setPlayerX(float pX)
{
	PlayerX = pX;
}

float tankPlayer::getPlayerX(void)
{
	return PlayerX;
}
void tankPlayer::setPlayerY(float pY)
{
	PlayerY = pY;
}
float tankPlayer::getPlayerY(void)
{
	return PlayerY;
}
void tankPlayer::setPlayerAngle(float pA)
{
	PlayerAngle += pA;
}
float tankPlayer::getPlayerAngle(void)
{
	return PlayerAngle;
}
void tankPlayer::setPlayerSpeed(float pS)
{
	PlayerSpeed = pS;
}
float tankPlayer::getPlayerSpeed(void)
{
	return PlayerSpeed;
}

void tankPlayer::setSpeedUp(bool sU)
{
	speedUp = sU;
}

bool tankPlayer::getSpeedUp(void)
{
	return speedUp;
}

void tankPlayer::setSlowDown(bool sD)
{
	slowDown = sD;
}

bool tankPlayer::getSlowDown(void)
{
	return slowDown;
}

void tankPlayer::setTurnLeft(bool tL)
{
	turnLeft = tL;
}

bool tankPlayer::getTurnLeft(void)
{
	return turnLeft;
}

void tankPlayer::setTurnRight(bool tR)
{
	turnRight = tR;
}

bool tankPlayer::getTurnRight(void)
{
	return turnRight;
}

void tankPlayer::setFireGun(bool fG)
{
	fireGun = fG;
}

bool tankPlayer::getFireGun(void)
{
	return fireGun;
}

void tankPlayer::setWPressed(bool wP)
{
	WPressed = wP;
}

bool tankPlayer::getWPressed(void)
{
	return WPressed;
}

void tankPlayer::setSPressed(bool sP)
{
	SPressed = sP;
}

bool tankPlayer::getSPressed(void)
{
	return SPressed;
}

tankPlayer::tankPlayer()
{
	tankBullet = new Bullet;
	tankTurret = new Turret;
	PlayerX = 0.0f;
	PlayerY = 0.0f;
	PlayerAngle = 0.0f;
	PlayerSpeed = 0.0f;
	speedUp = false;
	slowDown = false;
	turnLeft = false;
	turnRight = false;
	fireGun = false;
	WPressed = false;
	SPressed = false;
}